﻿using FluentAssertions;
using NSubstitute;
using Service_Tag_Reporter;
using System;
using System.Data;
using Xunit;

namespace ServiceTagTests
{
    public class ReportingTests
    {

        #region Variables

        DellSoapProcessor _soapEngine;
        string _validServiceTag;
        string _invalidServiceTag;
        string _badServiceTag;

        #endregion Variables

        #region Setup

        public ReportingTests()
        {
            _soapEngine = new DellSoapProcessor();

            _validServiceTag = "HPB7BQ1"; //generates functional tests
            _invalidServiceTag = "10101010101013348";
            _badServiceTag = "XXX000F";
        }

        #endregion Setup

        #region Private Methods

        #endregion Private Methods

        #region Tests

        [Fact]
        public void DellSoapProcessor_Setup_Succeeds()
        {
            _soapEngine.Should().NotBeNull();
        }

        [Fact]
        public void DellSoapProcessor_Setup_IntializesDataSet()
        {
            _soapEngine.DataTable.Should().NotBeNull();
        }

        [Fact]
        public void DellSoapProcessor_Given3Tags_Gives3Responses()
        {
            _soapEngine.Fetch(_badServiceTag);
            _soapEngine.Fetch(_badServiceTag);
            _soapEngine.Fetch(_badServiceTag);

            _soapEngine.DataTable.Rows.Count.Should().Be(3);
        }

        [Fact]
        public void DellSoapProcessor_GivenServiceTag_GeneratesResponse()
        {
            _soapEngine.Fetch(_badServiceTag);
            _soapEngine.DataTable.Rows.Count.Should().Be(1);
        }

        [Fact]
        public void DellSoapProcessor_GivenInvalidServiceTag_DoesNotGenerateResponse()
        {
            _soapEngine.Fetch(_invalidServiceTag);
            _soapEngine.DataTable.Rows.Count.Should().Be(0);
        }

        [Fact]
        public void DellSoapProcessor_ValidServiceTagResponse_IncludesModel()
        {
            _soapEngine.Fetch(_validServiceTag);
            _soapEngine.DataTable.Rows[0].ItemArray[0].Should().Be("Inspiron Desktop Inspiron One 2320");
        }

        [Fact]
        public void DellSoapProcessor_ValidServiceTagReponse_IncludesWarrantyDate()
        {
            _soapEngine.Fetch(_validServiceTag);
            _soapEngine.DataTable.Rows[0].ItemArray[2].ShouldBeEquivalentTo(DateTime.Parse("03/30/2020"));
        }

        [Fact]
        public void DellSoapProcessor_ValidServiceTagResponse_IncludesPurchaseDate()
        {
            _soapEngine.Fetch(_validServiceTag);
            _soapEngine.DataTable.Rows[0].ItemArray[1].ShouldBeEquivalentTo(DateTime.Parse("03/26/2012"));
        }

        [Fact]
        public void DellSoapProcessor_BadServiceTagResponse_IncludesUnkownModel()
        {
            _soapEngine.Fetch(_badServiceTag);
            _soapEngine.DataTable.Rows[0].ItemArray[0].ShouldBeEquivalentTo("Unknown Model");
        }

        [Fact]
        public void DellSoapProcessor_BadServiceTagReponse_IncludesMinWarrantyDate()
        {
            _soapEngine.Fetch(_badServiceTag);
            _soapEngine.DataTable.Rows[0].ItemArray[2].ShouldBeEquivalentTo(DateTime.MinValue);
        }

        [Fact]
        public void DellSoapProcessor_BadServiceTagResponse_IncludesMinPurchaseDate()
        {
            _soapEngine.Fetch(_badServiceTag);
            _soapEngine.DataTable.Rows[0].ItemArray[1].ShouldBeEquivalentTo(DateTime.MinValue);
        }

        [Fact]
        public void DellSoapProcessor_Reset_ClearsDataGrid()
        {
            _soapEngine.DataTable.Rows.Add("model", "01/01/2013", "01/01/2013");
            _soapEngine.DataTable.Rows.Count.Should().Be(1);

            _soapEngine.Reset();

            _soapEngine.DataTable.Rows.Count.Should().Be(0);
        }

        [Fact]
        public void DellSoapProcessor_Fetch_Stripswhitespace()
        {
            var firstTag = "HPB7BQ1\r"; 
            var secondTag = "HPB7BQ1  ";

            _soapEngine.Fetch(firstTag);
            _soapEngine.Fetch(secondTag);
            _soapEngine.DataTable.Rows.Count.Should().Be(2);
        }
        #endregion Tests
    }
}
