﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Service_Tag_Reporter
{
    public class DellSoapProcessor
    {
        #region Class Variables

        private DataTable _dataTable;

        //Constants
        //TODO: move to embedded resource file
        private const string _soapUrl = "http://xserv.dell.com/services/AssetService.asmx?WSDL";
        private const string _soapAction = "SOAPAction: \"http://support.dell.com/WebServices/GetAssetInformation\"";
        private const string _appName = "Service Tag Reporter";
        // {C8559A27-E1BD-40A7-85AF-C0624AC8E5C4}
        private Guid _clientID = new Guid(
            0xc8559a27, 0xe1bd, 0x40a7, 0x85, 0xaf, 0xc0, 0x62, 0x4a, 0xc8, 0xe5, 0xc4 );


        //Variables
        private string _xmlQuery;

        #endregion Class Variables

        #region Constructor

        public DellSoapProcessor()
        {
            _dataTable = new DataTable("Data");
            _dataTable.Columns.Add("ServiceTag");
            _dataTable.Columns.Add("Model");
            _dataTable.Columns.Add("Purchase Date");
            _dataTable.Columns.Add("Warranty Date");
        }

        #endregion Constructor

        #region Private Methods

        private string querySoapServer(string serviceTag)
        {
            //setup needed objects
            XmlDocument template = new XmlDocument();
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(_soapUrl);
            ASCIIEncoding encoder = new ASCIIEncoding();


            _xmlQuery = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n   <soap:Body>\r\n      <GetAssetInformation xmlns=\"http://support.dell.com/WebServices/\">\r\n         <guid>" + _clientID + "</guid>\r\n         <applicationName>" + _appName + "</applicationName>\r\n         <serviceTags>" + serviceTag + "</serviceTags>\r\n      </GetAssetInformation>\r\n   </soap:Body>\r\n</soap:Envelope>";
            byte[] writeBuffer = encoder.GetBytes(_xmlQuery);
            
            //format request
            request.Method = "POST";
            request.ContentLength = writeBuffer.Length;
            request.Headers.Add(_soapAction);
            request.ContentType = "text/xml; charset=utf-8";

            Stream netStream = request.GetRequestStream();
            netStream.Write(writeBuffer, 0, writeBuffer.Length);
            netStream.Close();

            //Send the request and pass back when complete
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            return reader.ReadToEnd();
        }

        private Tuple<string, DateTime, DateTime> formatXml(string serverResponse)
        {
            XmlDocument template = new XmlDocument();
            string text;
            string modelSeries = "Unknown";
            string modelNumber = "Model";
            DateTime dtWarrantyEnd = DateTime.MinValue;
            DateTime dtPurchaseDate = DateTime.MinValue;

            //Load unformatted XMl string into an Xml Template
            template.LoadXml(serverResponse);

            using (StringWriter strWriter = new StringWriter())
            {
                using (XmlTextWriter xmlWriter = new XmlTextWriter(strWriter))
                {
                    xmlWriter.Formatting = Formatting.Indented;
                    template.WriteTo(xmlWriter);
                }
                text = strWriter.ToString();
            }

            foreach(string s in text.Split('\n'))
            {
                string line = s.Trim();
                //Checks for End of Warranty tag
                if (line.StartsWith("<EndDate>"))
                {
                    //Dell Machines can have multiple warranties so we only want to take the last one
                    DateTime possibleDate = DateTime.Parse(line.Substring(9, 10));
                    if (possibleDate > dtWarrantyEnd)
                    {
                        dtWarrantyEnd = DateTime.Parse(line.Substring(9, 10));
                    }
                }
                else if (line.StartsWith("<SystemShipDate>")) //Check for purchase date
                {
                    dtPurchaseDate = DateTime.Parse(line.Substring(16, 10));
                }
                else if (line.StartsWith("<SystemType>")) //Checks for the model series
                {
                    modelSeries = line.Substring(12, (line.IndexOf("</SystemType>") - 12));
                }
                else if (line.StartsWith("<SystemModel>")) //Checks for the model number
                {
                    modelNumber = line.Substring(13, (line.IndexOf("</SystemModel>") - 13));
                }
            }

            return new Tuple<string, DateTime, DateTime>((modelSeries + " " + modelNumber), dtPurchaseDate, dtWarrantyEnd);
        }

        #endregion Private Methods

        #region Public Methods
        
        //public int Fetch(string serviceTag)
        //{
        //    serviceTag = serviceTag.Trim(); 
        //    if (serviceTag.Length >= 5 && serviceTag.Length <= 7)
        //    {
        //        string serverResponse = querySoapServer(serviceTag);
        //        Tuple<string, DateTime, DateTime> result = formatXml(serverResponse); 
        //        var newRow = _dataTable.NewRow();
        //        newRow["Model"] = result.Item1;
        //        newRow["Purchase Date"] = result.Item2;
        //        newRow["Warranty Date"] = result.Item3;

        //        _dataTable.Rows.Add(newRow);
        //        return 1;
        //    }          
        //    else       
        //    {
        //        return 0;
        //    }            
        //}

        public void Fetch(string serviceTag)
        {
            serviceTag = serviceTag.Trim();
            if (serviceTag.Length >= 5 && serviceTag.Length <= 7)
            {
                string serverResponse = querySoapServer(serviceTag);
                Tuple<string, DateTime, DateTime> result = formatXml(serverResponse);
                var newRow = _dataTable.NewRow();
                newRow["ServiceTag"] = serviceTag;
                newRow["Model"] = result.Item1;
                newRow["Purchase Date"] = result.Item2;
                newRow["Warranty Date"] = result.Item3;

                _dataTable.Rows.Add(newRow);
                //return 1;
            }
            else
            {
                //return 0;
            }
        }

        public void Reset()
        {
            DataTable.Rows.Clear();
        }

        #endregion Public Methods

        #region Public Properties

        public DataTable DataTable
        {
            get { return _dataTable; }
            set { _dataTable = value;  }
        }

        #endregion Public Properties
    }
}
