﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;

namespace Service_Tag_Reporter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SoapEngine = new DellSoapProcessor();
            grdWarrantyView.ItemsSource = SoapEngine.DataTable.DefaultView;
        }

        #region Public Properties

        public DellSoapProcessor SoapEngine { get; set; }

        #endregion Public Properties

        #region private methods

        private void fetchServiceTags()
        {

            List<Task> tasks = new List<Task>();
            foreach (var str in txtServiceTags.Text.Split('\n'))
            {
                Task t = Task.Factory.StartNew((arg) =>
                {
                    SoapEngine.Fetch(str);

                    Dispatcher.BeginInvoke(new queryDelegate(this.UpdateProgressBar), str);
                },

                    str
                );
                tasks.Add(t);
            }
            Task.WaitAll(tasks.ToArray());
            pbarLow.Maximum = tasks.Count;

        }

        private void UpdateProgressBar(string serial)
        {
            pbarLow.Value++;
            if (pbarLow.Value != pbarLow.Maximum)
            {
                lblStatus.Content = ("Fetching Service Information...");
            }
            else
            {
                lblStatus.Content = "Finished!";
            }
        }

        #endregion private methods

        #region Gui Events

        private void btnFetch_Click(object sender, RoutedEventArgs e)
        {
            SoapEngine.Reset();
            pbarLow.Value = 0;
            fetchServiceTags();
        }

        private delegate void queryDelegate(string serial);

        private void menuAbout_Click(object sender, RoutedEventArgs e)
        {
            var aboutWindow = new AboutWindow();
            aboutWindow.Show();
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            //pulls Serial number from target's WMI and dumps it into the serial box
            string strResults = null;
            string path = @"\\" + txtQueryTarget.Text + "\\root\\cimv2";

            try
            {
                ManagementScope wmiScope = new ManagementScope(path);
                ObjectQuery wmiOQuery = new ObjectQuery("SELECT SerialNumber FROM Win32_Bios");
                ManagementObjectSearcher wmiSearcher = new ManagementObjectSearcher(wmiScope, wmiOQuery);
                //Execute WMI query
                ManagementObjectCollection wmiResults = wmiSearcher.Get();
                foreach (ManagementObject Serial in wmiResults)
                {
                    strResults += Serial["SerialNumber"].ToString();
                }
                txtServiceTags.Text += strResults + "\n";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error scouting computer: " + ex.Message, "Error Locating Service Tag", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion Gui Events

    }
}
